mod lox;

use std::env;
use std::fs;
use std::io::BufRead;
use std::io::Write;
use std::process;

use lox::parsing;
use lox::scanning;

use lox::tokens::Token;

fn run_prompt() {
    let stdin = std::io::stdin();
    print!(">> ");
    std::io::stdout().flush().expect("Could not read line");
    for line in stdin.lock().lines() {
        match line {
            Ok(content) => run(content.as_str()),
            Err(e) => panic!(e),
        }
        print!(">> ");
        std::io::stdout().flush().expect("Could not read line");
    }
}

fn run_script(script: &str) {
    match fs::read_to_string(script) {
        Ok(content) => run(content.as_str()),
        Err(e) => panic!(e),
    };
}

fn run(content: &str) {
    let scanner_inst = scanning::Scanner::new(content);
    let lething: Vec<Result<Token, String>> = scanner_inst.collect();
    let mut oks = Vec::new();
    let mut errs = Vec::new();

    for item in lething {
        match item {
            Ok(v) => oks.push(v),
            Err(v) => errs.push(v),
        }
    }

    if !errs.is_empty() {
        println!("{:#?}", errs);
    } else {
        let parser_inst = parsing::ParserState::new(oks).expression();
        println!("{:#?}", parser_inst);
    }
}

#[allow(dead_code)]
fn error(line: i32, posn: &str, message: &str) {
    println!("[Line: {}] Error {}: {}", line, posn, message);
}

fn main() {
    let args: Vec<String> = env::args().collect();

    match args.as_slice() {
        [_] => run_prompt(),
        [_, script] => run_script(script),
        _ => {
            println!("Usage: jlox [script]");
            process::exit(64);
        }
    }
}
