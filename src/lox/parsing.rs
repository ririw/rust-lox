#![allow(dead_code)]

use crate::lox::tokens;
use crate::lox::tokens::TokenType;

#[derive(Debug, Clone)]
pub struct ParserState {
    toks: Vec<tokens::Token>,
    ptr: usize,
    parsed: Box<Option<Expr>>,
}
type PS = Result<ParserState, String>;

fn token_err<A>(token: tokens::Token) -> Result<A, String> {
    Err(format!(
        "Unexpected token {:#?} on line {:#?}",
        token, token.line
    ))
}

macro_rules! right_recursive_binary {
    ($name: ident, $left: ident, $cond: expr) => {
        pub fn $name(self) -> PS {
            let expr: ParserState = self.$left()?;
            let is_match = expr
                .peek()
                .map(|v| v.token_type)
                .map($cond)
                .unwrap_or(false);

            if is_match {
                let operator = expr.peek();
                let right = expr.accept()?.$name()?;
                let right_expr = right.parsed.clone().unwrap();
                Ok(right.add_parsed(Expr::ParseTreeBinary {
                    left: Box::new(expr.parsed.unwrap()),
                    token: operator.unwrap(),
                    right: Box::new(right_expr),
                }))
            } else {
                Ok(expr)
            }
        }
    };
}

impl ParserState {
    pub fn new(toks: Vec<tokens::Token>) -> Self {
        ParserState {
            toks: toks,
            ptr: 0,
            parsed: Box::new(None),
        }
    }

    fn no_inpt<A>(&self) -> Result<A, String> {
        let ln = self.toks.last().map(|v| v.line).unwrap_or(0);
        Err(format!("Error: ran out of input on line {:#?}", ln))
    }

    fn add_parsed(self, expr: Expr) -> Self {
        ParserState {
            toks: self.toks,
            ptr: self.ptr,
            parsed: Box::new(Some(expr)),
        }
    }

    fn peek(&self) -> Option<tokens::Token> {
        if self.ptr >= self.toks.len() {
            None
        } else {
            Some(self.toks[self.ptr].clone())
        }
    }

    fn accept(&self) -> PS {
        Ok(ParserState {
            toks: self.toks.clone(),
            ptr: self.ptr + 1,
            parsed: self.parsed.clone(),
        })
    }

    /*
    expression     → equality ;
    equality       → comparison ( ( "!=" | "==" ) comparison )* ;
    comparison     → addition ( ( ">" | ">=" | "<" | "<=" ) addition )* ;
    addition       → multiplication ( ( "-" | "+" ) multiplication )* ;
    multiplication → unary ( ( "/" | "*" ) unary )* ;
    unary          → ( "!" | "-" ) unary | primary ;
    primary        → NUMBER | STRING | "false" | "true" | "nil"
                   | "(" expression ")" ;
    */

    //expression     → equality ;
    pub fn expression(self) -> PS {
        self.equality()
    }

    //equality       → comparison ( ( "!=" | "==" ) comparison )* ;
    right_recursive_binary!(equality, comparison, |v| v == TokenType::TEqualEqual
        || v == TokenType::TBangEqual);
    //comparison     → addition ( ( ">" | ">=" | "<" | "<=" ) addition )* ;
    right_recursive_binary!(comparison, addition, |v| v == TokenType::TGreater
        || v == TokenType::TGreaterEqual
        || v == TokenType::TLess
        || v == TokenType::TLessEqual);
    //addition       → multiplication ( ( "-" | "+" ) multiplication )* ;
    right_recursive_binary!(addition, multiplication, |v| v == TokenType::TPlus
        || v == TokenType::TMinus);
    //multiplication → unary ( ( "/" | "*" ) unary )* ;
    right_recursive_binary!(multiplication, unary, |v| v == TokenType::TStar
        || v == TokenType::TSlash);

    //unary          → ( "!" | "-" ) unary | primary ;
    fn unary(self) -> PS {
        let operator = self.peek();
        let is_match = operator
            .map(|v| v.token_type)
            .map(|v| v == TokenType::TBang || v == TokenType::TMinus)
            .unwrap_or(false);
        if is_match {
            let rest = self.accept()?.unary()?;
            let parsed_v = rest.clone().parsed;
            let parsed = Expr::ParseTreeUnary {
                operator: self.peek().unwrap(),
                right: Box::new(parsed_v.unwrap()),
            };
            Ok(rest.add_parsed(parsed))
        } else {
            self.primary()
        }
    }

    fn check_is_match(&self, tok: TokenType) -> PS {
        match self.peek() {
            None => self.no_inpt(),
            Some(t) => {
                if t.token_type == tok {
                    self.accept()
                } else {
                    token_err(t)
                }
            }
        }
    }

    pub fn primary(self) -> PS {
        let _next = self.peek();
        if _next == None {
            return self.no_inpt();
        }
        let next = _next.unwrap();

        match next.token_type {
            TokenType::TFalse
            | TokenType::TTrue
            | TokenType::TNil
            | TokenType::TNumber
            | TokenType::TString => {
                let accepted = self.accept()?;
                let parse = Expr::ParseTreeLiteral {
                    value: next.clone(),
                };
                let parsed = accepted.add_parsed(parse);
                Ok(parsed)
            }
            TokenType::TLeftParen => {
                let accepted = self.accept()?;
                let parse = accepted.expression()?;
                let parsed = parse.clone().add_parsed(parse.parsed.unwrap());
                parsed.check_is_match(TokenType::TRightParen)
            }
            _ => token_err(next),
        }
    }
    fn synchronize(self) -> Self {
        let mut ptr = self.ptr + 1;
        let toks = &self.toks;

        while ptr < toks.len() {
            if toks[ptr - 1].token_type == tokens::TokenType::TSemicolon {
                break;
            }

            let passout = match toks[ptr].token_type {
                tokens::TokenType::TClass => true,
                tokens::TokenType::TFun => true,
                tokens::TokenType::TVar => true,
                tokens::TokenType::TFor => true,
                tokens::TokenType::TIf => true,
                tokens::TokenType::TWhile => true,
                tokens::TokenType::TPrint => true,
                tokens::TokenType::TReturn => true,
                _ => false,
            };

            if passout {
                break;
            }

            ptr += 1;
        }

        ParserState {
            toks: self.toks,
            ptr: ptr,
            parsed: Box::new(None),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
enum Expr {
    ParseTreeAssign {
        name: String,
        value: Box<Expr>,
    },
    ParseTreeBinary {
        left: Box<Expr>,
        token: tokens::Token,
        right: Box<Expr>,
    },
    ParseTreeCall {
        callee: Box<Expr>,
        paren: tokens::Token,
        args: Vec<Box<Expr>>,
    },
    ParseTreeGet {
        object: Box<Expr>,
        name: tokens::Token,
    },
    ParseTreeGrouping {
        expr: Box<Expr>,
    },
    ParseTreeLiteral {
        value: tokens::Token,
    },
    ParseTreeLogical {
        left: Box<Expr>,
        token: tokens::Token,
        right: Box<Expr>,
    },
    ParseTreeSet {
        object: Box<Expr>,
        token: tokens::Token,
        value: Box<Expr>,
    },
    ParseTreeSuper {
        keyword: tokens::Token,
        method: tokens::Token,
    },
    ParseTreeThis {
        keyword: tokens::Token,
    },
    ParseTreeUnary {
        operator: tokens::Token,
        right: Box<Expr>,
    },
    ParseTreeVariable {
        value: tokens::Token,
    },
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sync_semi() {
        let inpt = ParserState::new(vec![
            tokens::Token::new(tokens::TokenType::TPlus, "+", 0),
            tokens::Token::new(tokens::TokenType::TMinus, "-", 0),
            tokens::Token::new(tokens::TokenType::TSemicolon, ";", 0),
            tokens::Token::new(tokens::TokenType::TStar, "*", 0),
        ]);
        let res_toks = inpt.toks.clone();
        let res = inpt.synchronize();
        assert_eq!(res.toks, res_toks);
        assert_eq!(res.ptr, 3);
    }

    #[test]
    fn test_sync_class() {
        let inpt = ParserState::new(vec![
            tokens::Token::new(tokens::TokenType::TPlus, "+", 0),
            tokens::Token::new(tokens::TokenType::TMinus, "-", 0),
            tokens::Token::new(tokens::TokenType::TClass, "class", 0),
            tokens::Token::new(tokens::TokenType::TStar, "*", 0),
        ]);
        let res_toks = inpt.toks.clone();
        let res = inpt.synchronize();
        assert_eq!(res.toks, res_toks);
        assert_eq!(res.ptr, 2);
    }

    #[test]
    fn test_primary_num() {
        let res = ParserState::new(vec![tokens::Token::new(
            tokens::TokenType::TNumber,
            "1.32",
            0,
        )])
        .primary()
        .unwrap()
        .parsed;
        let exp = Some(Expr::ParseTreeLiteral {
            value: tokens::Token {
                token_type: TokenType::TNumber,
                lexeme: "1.32".to_string(),
                line: 0,
            },
        });
        assert_eq!(*res, exp)
    }

    #[test]
    fn test_primary_paren() {
        let res = ParserState::new(vec![
            tokens::Token::new(tokens::TokenType::TLeftParen, "(", 0),
            tokens::Token::new(tokens::TokenType::TNumber, "1.32", 0),
            tokens::Token::new(tokens::TokenType::TRightParen, ")", 0),
        ])
        .primary()
        .unwrap()
        .parsed;
        let exp = Some(Expr::ParseTreeLiteral {
            value: tokens::Token {
                token_type: TokenType::TNumber,
                lexeme: "1.32".to_string(),
                line: 0,
            },
        });
        assert_eq!(*res, exp)
    }

    #[test]
    fn test_primary_paren_unbal() {
        let res = ParserState::new(vec![
            tokens::Token::new(tokens::TokenType::TLeftParen, "(", 0),
            tokens::Token::new(tokens::TokenType::TNumber, "1.32", 0),
        ])
        .primary()
        .unwrap_err();
        assert_eq!(res, "Error: ran out of input on line 0")
    }

    #[test]
    fn test_unary_num() {
        let res = *ParserState::new(vec![
            tokens::Token::new(tokens::TokenType::TMinus, "-", 0),
            tokens::Token::new(tokens::TokenType::TNumber, "1.32", 0),
        ])
        .unary()
        .unwrap()
        .parsed;
        let exp = Some(Expr::ParseTreeUnary {
            operator: tokens::Token::new(tokens::TokenType::TMinus, "-", 0),
            right: Box::new(Expr::ParseTreeLiteral {
                value: tokens::Token {
                    token_type: TokenType::TNumber,
                    lexeme: "1.32".to_string(),
                    line: 0,
                },
            }),
        });
        assert_eq!(exp, res);
    }

    #[test]
    fn test_mul() {
        let res = *ParserState::new(vec![
            tokens::Token::new(tokens::TokenType::TMinus, "-", 0),
            tokens::Token::new(tokens::TokenType::TNumber, "1.32", 0),
            tokens::Token::new(tokens::TokenType::TStar, "*", 0),
            tokens::Token::new(tokens::TokenType::TNumber, "4", 0),
            tokens::Token::new(tokens::TokenType::TSlash, "/", 0),
            tokens::Token::new(tokens::TokenType::TNumber, "3", 0),
        ])
        .multiplication()
        .unwrap()
        .parsed;
        match res.unwrap() {
            Expr::ParseTreeBinary {
                left: _,
                token: v,
                right: _,
            } => assert_eq!(v.token_type, tokens::TokenType::TStar),
            _ => unimplemented!(),
        };
    }

}
