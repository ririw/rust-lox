use crate::lox::tokens::{Token, TokenType};

#[derive(Debug, Clone)]
pub struct Scanner {
    content: Vec<char>,
    current: usize,
    line_num: u32,
    has_err: bool,
}

fn lookup_identifier(identifier: &str) -> TokenType {
    match identifier {
        "and" => TokenType::TAnd,
        "class" => TokenType::TClass,
        "else" => TokenType::TElse,
        "false" => TokenType::TFalse,
        "for" => TokenType::TFor,
        "fun" => TokenType::TFun,
        "if" => TokenType::TIf,
        "nil" => TokenType::TNil,
        "or" => TokenType::TOr,
        "print" => TokenType::TPrint,
        "return" => TokenType::TReturn,
        "super" => TokenType::TSuper,
        "this" => TokenType::TThis,
        "true" => TokenType::TTrue,
        "var" => TokenType::TVar,
        "while" => TokenType::TWhile,
        _ => TokenType::TIdentifier,
    }
}

impl Scanner {
    pub fn new(content: &str) -> Scanner {
        return Scanner {
            content: content.chars().collect(),
            current: 0,
            line_num: 0,
            has_err: false,
        };
    }

    fn absorb_whitespace(&mut self) {
        while !self.is_at_end() {
            let cur_char = self.content[self.current];
            if cur_char == '\n' {
                self.line_num += 1
            }
            if cur_char != '\r' && cur_char != '\t' && cur_char != ' ' && cur_char != '\n' {
                return;
            }
            self.current += 1;
        }
    }

    fn ingest_identifier(&mut self, fc: char) -> Result<Option<Token>, String> {
        let mut accum = String::from("");
        accum.push(fc);

        while !self.is_at_end() && Scanner::is_alpha_num(self.content[self.current]) {
            accum.push(self.advance().unwrap());
        }

        Ok(Some(Token::new_from_string(
            lookup_identifier(&accum),
            accum,
            self.line_num,
        )))
    }

    fn scan_token(&mut self) -> Result<Option<Token>, String> {
        self.absorb_whitespace();
        let c = self.advance();

        if c == Some('/') && self.do_match('/') {
            self.ingest_comment();
            self.scan_token()
        } else if c.map_or(false, |v| Scanner::is_digit(v)) {
            self.ingest_number(c.unwrap())
        } else if c.map_or(false, |v| Scanner::is_alpha_num(v)) {
            self.ingest_identifier(c.unwrap())
        } else {
            match c {
                None => Ok(None),
                Some('(') => Ok(Some(Token::new(TokenType::TLeftParen, "(", self.line_num))),
                Some(')') => Ok(Some(Token::new(TokenType::TRightParen, ")", self.line_num))),
                Some('{') => Ok(Some(Token::new(TokenType::TLeftBrace, "{", self.line_num))),
                Some('}') => Ok(Some(Token::new(TokenType::TRightBrace, "}", self.line_num))),
                Some(',') => Ok(Some(Token::new(TokenType::TComma, ",", self.line_num))),
                Some('.') => Ok(Some(Token::new(TokenType::TDot, ".", self.line_num))),
                Some('-') => Ok(Some(Token::new(TokenType::TMinus, "-", self.line_num))),
                Some('+') => Ok(Some(Token::new(TokenType::TPlus, "+", self.line_num))),
                Some(';') => Ok(Some(Token::new(TokenType::TSemicolon, ";", self.line_num))),
                Some('*') => Ok(Some(Token::new(TokenType::TStar, "*", self.line_num))),
                Some('!') => {
                    if self.do_match('=') {
                        Ok(Some(Token::new(TokenType::TBangEqual, "!=", self.line_num)))
                    } else {
                        Ok(Some(Token::new(TokenType::TBang, "!", self.line_num)))
                    }
                }
                Some('=') => {
                    if self.do_match('=') {
                        let tok = TokenType::TEqualEqual;
                        Ok(Some(Token::new(tok, "==", self.line_num)))
                    } else {
                        Ok(Some(Token::new(TokenType::TEqual, "=", self.line_num)))
                    }
                }
                Some('<') => {
                    if self.do_match('=') {
                        Ok(Some(Token::new(TokenType::TLessEqual, "<=", self.line_num)))
                    } else {
                        Ok(Some(Token::new(TokenType::TLess, "<", self.line_num)))
                    }
                }
                Some('>') => {
                    if self.do_match('=') {
                        let tok = TokenType::TGreaterEqual;
                        Ok(Some(Token::new(tok, ">=", self.line_num)))
                    } else {
                        Ok(Some(Token::new(TokenType::TGreater, ">", self.line_num)))
                    }
                }
                Some('/') => {
                    if self.do_match('/') {
                        Ok(Some(self.ingest_comment()))
                    } else {
                        Ok(Some(Token::new(TokenType::TSlash, "/", self.line_num)))
                    }
                }
                Some('"') => self.read_string(),
                Some(c) => {
                    self.has_err = true;
                    Err(format!("Unrecognised token {}", c))
                }
            }
        }
    }

    fn is_digit(c: char) -> bool {
        return c >= '0' && c <= '9';
    }

    fn ingest_number(&mut self, fc: char) -> Result<Option<Token>, String> {
        let mut accum = String::from("");
        accum.push(fc);

        while !self.is_at_end() && Scanner::is_digit(self.content[self.current]) {
            accum.push(self.advance().unwrap());
        }
        if self.peek_check('.') {
            accum.push(self.advance().unwrap());
        }
        while !self.is_at_end() && Scanner::is_digit(self.content[self.current]) {
            accum.push(self.advance().unwrap());
        }

        Ok(Some(Token::new_from_string(
            TokenType::TNumber,
            accum,
            self.line_num,
        )))
    }

    fn is_alpha(c: char) -> bool {
        let is_lc = c >= 'a' && c <= 'z';
        let is_uc = c >= 'A' && c <= 'Z';
        let is__ = c == '_';
        return is_lc || is_uc || is__;
    }

    fn is_alpha_num(c: char) -> bool {
        return Scanner::is_alpha(c) || Scanner::is_digit(c);
    }

    fn read_string(&mut self) -> Result<Option<Token>, String> {
        let mut accum = String::from("");
        let line_num = self.line_num;
        while !self.peek_check('"') && !self.is_at_end() {
            if self.peek_check('\n') {
                self.line_num += 1
            }
            self.advance().map(|c| accum.push(c));
        }

        if self.is_at_end() {
            self.has_err = true;
            Err(String::from("Unterminated string."))
        } else {
            assert!(self.advance() == Some('"'));
            Ok(Some(Token::new_from_string(
                TokenType::TString,
                accum,
                line_num,
            )))
        }
    }

    fn ingest_comment(&mut self) -> Token {
        let line_num = self.line_num;
        let mut accum = String::from("");
        while !self.is_at_end() && self.content[self.current] != '\n' {
            accum.push(self.content[self.current]);
            self.current += 1
        }
        // Consume and "\n" remaining.
        if !self.is_at_end() {
            self.current += 1;
            self.line_num += 1;
        }

        Token::new_from_string(TokenType::TComment, accum, line_num)
    }

    fn do_match(&mut self, item: char) -> bool {
        if self.is_at_end() {
            false
        } else if self.content[self.current] != item {
            false
        } else {
            self.current += 1;
            true
        }
    }

    fn peek_check(&mut self, item: char) -> bool {
        if self.is_at_end() {
            false
        } else {
            self.content[self.current] == item
        }
    }

    fn advance(&mut self) -> Option<char> {
        if self.is_at_end() {
            None
        } else {
            self.current += 1;
            Some(self.content[self.current - 1])
        }
    }

    fn is_at_end(&mut self) -> bool {
        self.current >= self.content.len()
    }
}

impl Iterator for Scanner {
    type Item = Result<Token, String>;
    fn next(&mut self) -> Option<Result<Token, String>> {
        match self.scan_token() {
            Ok(None) => None,
            Ok(Some(v)) => Some(Ok(v)),
            Err(v) => Some(Err(v)),
        }
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;
    use std::collections::HashMap;

    #[test]
    fn test_empty() {
        let content = "";
        let mut scanner = Scanner::new(content);
        assert_eq!(scanner.scan_token(), Ok(None))
    }

    #[test]
    fn test_someerr() {
        let content = "^";
        let mut scanner = Scanner::new(content);
        assert_eq!(
            scanner.scan_token(),
            Err(String::from("Unrecognised token ^"))
        )
    }

    #[test]
    fn test_single() {
        let mut char_matches = HashMap::new();
        char_matches.insert('(', TokenType::TLeftParen);
        char_matches.insert(')', TokenType::TRightParen);
        char_matches.insert('{', TokenType::TLeftBrace);
        char_matches.insert('}', TokenType::TRightBrace);
        char_matches.insert(',', TokenType::TComma);
        char_matches.insert('.', TokenType::TDot);
        char_matches.insert('-', TokenType::TMinus);
        char_matches.insert('+', TokenType::TPlus);
        char_matches.insert('*', TokenType::TStar);
        char_matches.insert(';', TokenType::TSemicolon);
        char_matches.insert('/', TokenType::TSlash);

        for (char, tok) in char_matches.iter() {
            let content = format!("{}qwe", char);
            let expected_cont = format!("{}", char);
            let expected = Ok(Some(Token::new(*tok, &expected_cont, 0)));
            let mut scanner = Scanner::new(&content);
            assert_eq!(scanner.scan_token(), expected);
            assert_eq!(scanner.current, 1);
            assert_eq!(scanner.line_num, 0);
        }
    }

    #[test]
    fn test_comments_1() {
        let content = String::from("// hello world");
        let expected = Ok(None);
        let mut scanner = Scanner::new(&content);
        assert_eq!(scanner.scan_token(), expected);
        assert_eq!(scanner.current, 14);
        assert_eq!(scanner.line_num, 0);
    }

    #[test]
    fn test_comments_2() {
        let content = String::from("// hello world\n*");
        let tok = TokenType::TStar;
        let expected = Ok(Some(Token::new(tok, "*", 1)));
        let mut scanner = Scanner::new(&content);
        assert_eq!(scanner.scan_token(), expected);
        assert_eq!(scanner.current, 16);
        assert_eq!(scanner.line_num, 1);
    }

    #[test]
    fn test_example() {
        let content = String::from("(( ){} // test");
        let mut scanner = Scanner::new(&content);
        assert_eq!(
            scanner.scan_token().map(|v| v.map(|x| x.token_type)),
            Ok(Some(TokenType::TLeftParen))
        );
        assert_eq!(
            scanner.scan_token().map(|v| v.map(|x| x.token_type)),
            Ok(Some(TokenType::TLeftParen))
        );
        assert_eq!(
            scanner.scan_token().map(|v| v.map(|x| x.token_type)),
            Ok(Some(TokenType::TRightParen))
        );
        assert_eq!(
            scanner.scan_token().map(|v| v.map(|x| x.token_type)),
            Ok(Some(TokenType::TLeftBrace))
        );
        assert_eq!(
            scanner.scan_token().map(|v| v.map(|x| x.token_type)),
            Ok(Some(TokenType::TRightBrace))
        );
        assert_eq!(scanner.scan_token(), Ok(None));
    }

    #[test]
    fn test_str() {
        let content = String::from("\"hello\"*");
        let mut scanner = Scanner::new(&content);
        assert_eq!(
            scanner.scan_token().map(|v| v.map(|x| x.token_type)),
            Ok(Some(TokenType::TString))
        );
        assert_eq!(
            scanner.scan_token().map(|v| v.map(|x| x.token_type)),
            Ok(Some(TokenType::TStar))
        );

        let mut scanner = Scanner::new(&content);
        assert_eq!(
            scanner.scan_token().map(|v| v.map(|x| x.lexeme)),
            Ok(Some(String::from("hello")))
        );
    }

    #[test]
    fn test_str_err() {
        let content = String::from("\"hello");
        let mut scanner = Scanner::new(&content);
        assert_eq!(
            scanner.scan_token(),
            Err(String::from("Unterminated string."))
        );
    }

    #[test]
    fn test_nums() {
        let content = String::from("12");
        let mut scanner = Scanner::new(&content);
        assert_eq!(
            scanner.scan_token(),
            Ok(Some(Token::new(TokenType::TNumber, "12", 0)))
        );

        let content = String::from("12.");
        let mut scanner = Scanner::new(&content);
        assert_eq!(
            scanner.scan_token(),
            Ok(Some(Token::new(TokenType::TNumber, "12.", 0)))
        );

        let content = String::from("12.3");
        let mut scanner = Scanner::new(&content);
        assert_eq!(
            scanner.scan_token(),
            Ok(Some(Token::new(TokenType::TNumber, "12.3", 0)))
        );
        let content = String::from("12.3\n+");
        let mut scanner = Scanner::new(&content);
        assert_eq!(
            scanner.scan_token(),
            Ok(Some(Token::new(TokenType::TNumber, "12.3", 0)))
        );
        assert_eq!(
            scanner.scan_token(),
            Ok(Some(Token::new(TokenType::TPlus, "+", 1)))
        );
    }

    #[test]
    fn test_is_alpha() {
        assert!(Scanner::is_alpha('a'));
        assert!(Scanner::is_alpha('A'));
        assert!(Scanner::is_alpha('z'));
        assert!(Scanner::is_alpha('Z'));
        assert!(Scanner::is_alpha('m'));
        assert!(Scanner::is_alpha('M'));
        assert!(Scanner::is_alpha('_'));
        assert!(!Scanner::is_alpha('0'));
        assert!(!Scanner::is_alpha('!'));
        assert!(!Scanner::is_alpha('.'));
        assert!(!Scanner::is_alpha('🦖'));
    }

    #[test]
    fn test_is_alpha_num() {
        assert!(Scanner::is_alpha_num('a'));
        assert!(Scanner::is_alpha_num('A'));
        assert!(Scanner::is_alpha_num('z'));
        assert!(Scanner::is_alpha_num('Z'));
        assert!(Scanner::is_alpha_num('m'));
        assert!(Scanner::is_alpha_num('M'));
        assert!(Scanner::is_alpha_num('_'));
        assert!(Scanner::is_alpha_num('0'));
        assert!(Scanner::is_alpha_num('4'));
        assert!(Scanner::is_alpha_num('9'));
        assert!(!Scanner::is_alpha_num('!'));
        assert!(!Scanner::is_alpha_num('.'));
        assert!(!Scanner::is_alpha_num('🦖'));
    }

    #[test]
    fn test_identifier() {
        let content = "+hello";
        let mut scanner = Scanner::new(content);
        assert_eq!(
            scanner.scan_token(),
            Ok(Some(Token {
                token_type: TokenType::TPlus,
                lexeme: "+".to_string(),
                line: 0
            }))
        );
        assert_eq!(
            scanner.scan_token(),
            Ok(Some(Token {
                token_type: TokenType::TIdentifier,
                lexeme: "hello".to_string(),
                line: 0
            }))
        )
    }

    #[test]
    fn test_show_ident() {
        let content = "+and";
        let mut scanner = Scanner::new(content);
        assert_eq!(
            scanner.scan_token(),
            Ok(Some(Token {
                token_type: TokenType::TPlus,
                lexeme: "+".to_string(),
                line: 0
            }))
        );
        assert_eq!(
            scanner.scan_token(),
            Ok(Some(Token {
                token_type: TokenType::TAnd,
                lexeme: "and".to_string(),
                line: 0
            }))
        )
    }

    #[test]
    fn test_show_ident_mode() {
        let content = "and or\nbest";
        let mut scanner = Scanner::new(content);
        assert_eq!(
            scanner.scan_token(),
            Ok(Some(Token {
                token_type: TokenType::TAnd,
                lexeme: "and".to_string(),
                line: 0
            }))
        );
        assert_eq!(
            scanner.scan_token(),
            Ok(Some(Token {
                token_type: TokenType::TOr,
                lexeme: "or".to_string(),
                line: 0
            }))
        );
        assert_eq!(
            scanner.scan_token(),
            Ok(Some(Token {
                token_type: TokenType::TIdentifier,
                lexeme: "best".to_string(),
                line: 1
            }))
        );
        assert_eq!(scanner.scan_token(), Ok(None))
    }
}
