#[allow(dead_code)]
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum TokenType {
    // Single-character tokens.
    TLeftParen,
    TRightParen,
    TLeftBrace,
    TRightBrace,
    TComma,
    TDot,
    TMinus,
    TPlus,
    TSemicolon,
    TSlash,
    TStar,

    // One or two character tokens.
    TBang,
    TBangEqual,
    TEqual,
    TEqualEqual,
    TGreater,
    TGreaterEqual,
    TLess,
    TLessEqual,

    // Literals.
    TIdentifier,
    TString,
    TNumber,

    // Keywords.
    TAnd,
    TClass,
    TElse,
    TFalse,
    TFun,
    TFor,
    TIf,
    TNil,
    TOr,
    TPrint,
    TReturn,
    TSuper,
    TThis,
    TTrue,
    TVar,
    TWhile,

    TEof,

    TOther,
    TComment,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Token {
    pub token_type: TokenType,
    pub lexeme: String,
    pub line: u32,
}

impl Token {
    pub fn new<'a>(token: TokenType, lexeme: &'a str, line: u32) -> Token {
        return Token {
            token_type: token,
            lexeme: String::from(lexeme),
            line: line,
        };
    }

    pub fn new_from_string(token: TokenType, lexeme: String, line: u32) -> Token {
        return Token {
            token_type: token,
            lexeme: lexeme,
            line: line,
        };
    }
}
